#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include "global.h"
#include "match.h"
#include "usb_keyboard.h"

char m_aUsbKey[SIZE] = "0"; 
char m_aRelMat[SIZE] = "0";  
char m_aIpAd[SIZE] = "0";
const char c_aHoms[SIZE] = "homesuccess";
const char c_aFris[SIZE] = "friendsuccess";
const char c_aHomf[SIZE] = "homefailed";
const char c_aFrif[SIZE] = "friendsfailed";
void openDoor_BUZZ(int fw,int fre)
{
		ioctl(fw,FSPWM_START,0);//蜂鸣器开
		ioctl(fw,FSPWM_SET_FREQ,fre);
		usleep(300000);
		ioctl(fw,FSPWM_STOP,0);//蜂鸣器关
}
void closeDoor_BUZZ(int fw,int fre)
{
		ioctl(fw,FSPWM_START,0);//蜂鸣器开
		ioctl(fw,FSPWM_SET_FREQ,fre);
		usleep(300000);
		ioctl(fw,FSPWM_STOP,0);//蜂鸣器关
		usleep(200000);
		ioctl(fw,FSPWM_START,0);//蜂鸣器开
		ioctl(fw,FSPWM_SET_FREQ,fre);
		usleep(300000);
		ioctl(fw,FSPWM_STOP,0);//蜂鸣器关
}
int main(int argc, char **argv)
{
	int fd,fw,i;
	while(1)
	{
		fd = open("/dev/led0", O_RDWR);
		fw = open("/dev/pwm", O_RDWR);
		if (fd < 0 || fw < 0)
		{
			perror("open");
			exit(1);
		}
		memset(m_aUsbKey,0,sizeof(m_aUsbKey));
		memset(m_aRelMat,0,sizeof(m_aRelMat));
		a9_usb_keyboard();
		a9_sqlite3_match();
		
		if(0 == strcmp(m_aRelMat,"homesuccess")) //led4亮开门延时10秒 关闭 之后led5亮
		{
			ioctl(fd, LED5_OFF);
			ioctl(fd, LED2_OFF);
			ioctl(fd, LED4_ON);
			openDoor_BUZZ(fw,400);
			sleep(10);
			ioctl(fd, LED4_OFF);
			ioctl(fd, LED5_ON);
			closeDoor_BUZZ(fw,400);	
		}
		else if (0 == strcmp(m_aRelMat,"homefailed"))//led2亮  pwn 报警
		{
		
			ioctl(fd, LED5_ON);
			ioctl(fd, LED4_OFF);
			ioctl(fd, LED2_ON);
			
			ioctl(fw,FSPWM_START,0);//蜂鸣器开
		    ioctl(fw,FSPWM_SET_FREQ,262);
			sleep(15);
			ioctl(fw,FSPWM_STOP,0);//蜂鸣器关
			ioctl(fd, LED2_OFF);
		
		}
		else if(0 == strcmp(m_aRelMat,"friendsuccess"))//led4亮开门 延时10秒 灭  led5亮关门
		{
			ioctl(fd, LED5_OFF);
			ioctl(fd, LED2_OFF);
		    ioctl(fd, LED4_ON);
			openDoor_BUZZ(fw,400);
			sleep(10);
			ioctl(fd, LED4_OFF);
			ioctl(fd, LED5_ON);
			closeDoor_BUZZ(fw,400);
		
		}
		else if (0 == strcmp(m_aRelMat,"friendfailed"))//led2亮  pwn 报警   ，led5亮关门
		{
			ioctl(fd, LED5_ON);
			ioctl(fd, LED4_OFF);
			ioctl(fd, LED2_ON);
		
			ioctl(fw,FSPWM_START,0);//蜂鸣器开
		    ioctl(fw,FSPWM_SET_FREQ,262);
			sleep(15);
			ioctl(fw,FSPWM_STOP,0);//蜂鸣器关
			
			ioctl(fd, LED2_OFF);
		
		}
		close(fd);
		close(fw);
	}		
	return 0;
}
