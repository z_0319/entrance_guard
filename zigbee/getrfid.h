#ifndef __GETRFID_H__
#define __GETRFID_H__
#define SIZE 256

int zigbee_get_rfid_data(int fd);

#endif