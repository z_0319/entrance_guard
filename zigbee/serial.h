#ifndef   __SERIAL_H__
#define  __SERIAL_H__


struct env{
	int id;
	int tem;
	int hum;
	int light;
	int x;
	int y;
	int z;
};

struct env data;

/**
 * 打开串口设备
 * @param[in] devpath 串口设备路径
 * @return 打开的文件描述符
 * @retval 0 打开设备失败，原因见errno
 */
int serial_open(char *devpath)  ;

void serial_Close(int fd)  ;
int serial_Set(int fd, int speed, int flow_ctrl, int databits, int stopbits, int parity) ;
int serial_init(char *devpath)  ;
int serial_recv_exact_nbytes(int fd, void *buf, int count);
int serial_send_exact_nbytes(int fd, unsigned char *buf, int count);
int serial_exit(int fd);







#endif
