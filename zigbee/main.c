#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <termios.h>
#include <pthread.h>

#include "serial.h"
#include "getrfid.h"
#define ZIGBEE_DEV_PATH "/dev/ttyUSB0"               
char m_aRfidNu[SIZE];                               
int ttyUSB0_fd;


int main(int argc, char *argv[])
{
	
	
	ttyUSB0_fd = serial_init(ZIGBEE_DEV_PATH);
	if (-1 == ttyUSB0_fd)
	{
		printf("open ttyUSB0 failed!\n");
	}
	
	while(1)
	{
		
		int ret = zigbee_get_rfid_data(ttyUSB0_fd);
		if (ret < 0)
		{
			printf("no data!\n");
		}
		printf ("rfid num = %s\n",m_aRfidNu);
	}
	 
	return 0;
}