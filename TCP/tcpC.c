#include "tcpC.h"

void ContexA9_Client(void)
{
	int socketID  = 0;
	struct sockaddr_in serv_addr;
	int addrlen = 0;
	int ret = 0;
	char buf[SIZE] = {0};
   // scanf("%s\n",buf);
    gets(buf);
	//char buf[SIZE] = {0};
	//打开 socket  -- 协议
	socketID = socket(AF_INET, SOCK_STREAM, 0);
	if (socketID < 0)
	{
		perror("socket error");
	//	return -1;
	}
	printf("socket ok\r\n");
	
	struct sockaddr_in myAddr;
	addrlen = sizeof(serv_addr);
	memset(&myAddr, 0, addrlen);
	myAddr.sin_family = AF_INET;
	myAddr.sin_port = htons(PORT + 1);
	myAddr.sin_addr.s_addr = INADDR_ANY;
	ret = bind (socketID, (struct sockaddr*)&myAddr, addrlen);
	if (ret < 0)
	{
		perror("bind error");
		close(socketID);
	//	return -1;
		return;
	}
	printf("bind ok\r\n");
	
	//设置服务器的地址（ip/port）
	addrlen = sizeof(serv_addr);
	memset(&serv_addr, 0, addrlen);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);
	serv_addr.sin_addr.s_addr = inet_addr(IP);
	//发送连接请求
	ret = connect(socketID, (struct sockaddr*)&serv_addr, addrlen);
	if (ret < 0)
	{
		perror("connect error");
		close(socketID);
	//	return -1;
		return ;
	}
	printf("connect ok\r\n");
	
	//通信
	if (0 < write(socketID, buf, strlen(buf)))
	{
		printf("write ok\r\n");
	}
	memset(buf, 0, SIZE);
    recv(socketID, buf, SIZE - 1, 0);
	printf("recv : %s\r\n", buf);

	//关闭socket 
	close(socketID);
	
}




