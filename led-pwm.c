#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>


#define LED2_MAGIC 'L'
#define LED3_MAGIC 'M'
#define LED4_MAGIC 'N'
#define LED5_MAGIC 'O'

#define FSPWM_MAGIC	'f'
#define FSPWM_START	  _IO(FSPWM_MAGIC, 0)
#define FSPWM_STOP	  _IO(FSPWM_MAGIC, 1)
#define FSPWM_SET_FREQ	_IOW(FSPWM_MAGIC, 2, unsigned int)
#define data 10000

#define API_ENTER() printk("func: %s at line: %d is called\r\n", __FUNCTION__, __LINE__);
#define API_EXIT()  printk("func: %s exit\r\n", __FUNCTION__);

#define LED_ON	    _IOW(LED_MAGIC, 0, int)
#define LED_OFF	_IOW(LED_MAGIC, 1, int)
// LED 2亮表示（出错，并且pwd蜂鸣器响） LED4亮（表示开门） LED5亮（表示关门）


#define LED2_ON 	_IOW(LED2_MAGIC, 0, int)
#define LED2_OFF	_IOW(LED2_MAGIC, 1, int)
//#define LED3_ON	_IOW(LED3_MAGIC, 0, int)
//#define LED3_OFF	_IOW(LED3_MAGIC, 1, int)
#define LED4_ON	   _IOW(LED4_MAGIC, 0, int)
#define LED4_OFF  	_IOW(LED4_MAGIC, 1, int)
#define LED5_ON	   _IOW(LED5_MAGIC, 0, int)
#define LED5_OFF	_IOW(LED5_MAGIC, 1, int)

#define SIZE  100
#define DEVICE_COUNT 4

char str[20];//如果是friendsuccess则开门  如果是homesuccess则开门
char str1[20];//

int main(int argc, char **argv)
{
	int fd,fw,i;
	char f_str[20];//接收匹配信息
	
	fd = open("/dev/led0", O_RDWR);
	fw = open("/dev/pwm", O_RDWR);
	if (fd < 0 && fw < 0)
	{
		perror("open");
		exit(1);
	}
	
	
	int ret;
	unsigned int freq;
	
	
	
	//ret = ioctl(fd,FSPWM_START,0);//打开蜂鸣器
	
	//ret = ioctl(fd,FSPWM_SET_FREQ,262);
	//sleep(5);
	//ret = ioctl(fd,FSPWM_STOP,0);
	while(1)
	{
		printf("请输入字符串\n");//测试
		scanf("%s",str);

		if(0 == strcmp(str,"homesuccess")) //led4亮开门延时10秒 关闭 之后led5亮
		{
			ioctl(fd, LED5_OFF);
			ioctl(fd, LED2_OFF);
			ioctl(fd, LED4_ON);
			sleep(10);
			ioctl(fd, LED4_OFF);
			ioctl(fd, LED5_ON);
		
		}
		else if (0 == strcmp(str,"homefailed"))//led2亮  pwn 报警
		{
		
			ioctl(fd, LED5_ON);
			ioctl(fd, LED4_OFF);
			ioctl(fd, LED2_ON);
			
			ioctl(fw,FSPWM_START,0);//蜂鸣器开
		    ioctl(fw,FSPWM_SET_FREQ,262);
			sleep(15);
			ioctl(fw,FSPWM_STOP,0);//蜂鸣器关
			ioctl(fd, LED2_OFF);
		
		}
		else if(0 == strcmp(str,"friendsuccess"))//led4亮开门 延时10秒 灭  led5亮关门
		{
			//需要tcp客户端发送的命令
			ioctl(fd, LED5_OFF);
			ioctl(fd, LED2_OFF);
		    ioctl(fd, LED4_ON);
			sleep(10);
			ioctl(fd, LED4_OFF);
			ioctl(fd, LED5_ON);
		
		}
		else if (0 == strcmp(str,"friendfailed"))//led2亮  pwn 报警   ，led5亮关门
		{
			ioctl(fd, LED5_ON);
			ioctl(fd, LED4_OFF);
			ioctl(fd, LED2_ON);
		
			ioctl(fw,FSPWM_START,0);//蜂鸣器开
		    ioctl(fw,FSPWM_SET_FREQ,262);
			sleep(15);
			ioctl(fw,FSPWM_STOP,0);//蜂鸣器关
			
			ioctl(fd, LED2_OFF);
		
		}
		memset(str,0,sizeof(str));
	}
		

	return 0;
}
