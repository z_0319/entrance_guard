#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#define LED2_MAGIC 'L'
#define LED3_MAGIC 'M'
#define LED4_MAGIC 'N'
#define LED5_MAGIC 'O'

#define LED2_ON	_IOW(LED2_MAGIC, 0, int)
#define LED2_OFF	_IOW(LED2_MAGIC, 1, int)
#define LED3_ON	_IOW(LED3_MAGIC, 0, int)
#define LED3_OFF	_IOW(LED3_MAGIC, 1, int)
#define LED4_ON	_IOW(LED4_MAGIC, 0, int)
#define LED4_OFF	_IOW(LED4_MAGIC, 1, int)
#define LED5_ON	_IOW(LED5_MAGIC, 0, int)
#define LED5_OFF	_IOW(LED5_MAGIC, 1, int)
#define INPUT_NAME "Keyboard"

#define FSPWM_MAGIC	'f'
#define FSPWM_START	  _IO(FSPWM_MAGIC, 0)
#define FSPWM_STOP	  _IO(FSPWM_MAGIC, 1)
#define FSPWM_SET_FREQ	_IOW(FSPWM_MAGIC, 2, unsigned int)

#define SIZE 256


extern char m_aUsbKey[SIZE]; 
extern char m_aRelMat[SIZE];  
extern char m_aIpAd[SIZE];
extern char m_aRecvBuf[SIZE];
extern const char c_aHoms[SIZE];
extern const char c_aFris[SIZE];
extern const char c_aHomf[SIZE];
extern const char c_aFrif[SIZE];
void ContexA9_Client(char m_aRelMat[SIZE]);

#endif
