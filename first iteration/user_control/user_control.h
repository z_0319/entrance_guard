#ifndef USER_CONTROL_H
#define USER_CONTROL_H

#include <QWidget>
#include <QTcpServer>
#include <qtcpsocket.h>

#include <QMediaPlayer>
#include <QTimer>

namespace Ui {
class user_control;
}

class user_control : public QWidget
{
    Q_OBJECT

public:
    explicit user_control(QWidget *parent = 0);
    ~user_control();

private slots:

    void on_open_clicked();

    void on_exit_clicked();

    void controlconnect();

    void recvMovieData();

    void selectRemindMusic();

   // void musicName();

    //void musicVolume(int);
    //void openMusicPath();

private:
    Ui::user_control *ui;
    QTcpServer *user_tcpserver;
    QTcpSocket *user_tcpsocket;
    QMediaPlayer *player;
    QTimer *timer;
    int n;
    QString musicPath;
    QString MusicName;
};

#endif // USER_CONTROL_H
