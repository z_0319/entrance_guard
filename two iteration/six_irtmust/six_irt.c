#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "global.h"
#include "match.h"
#include "usb_keyboard.h"

#include <stdint.h>
#include <errno.h>
#include <termios.h>
#include <pthread.h>
#include "serial.h"
#include "getrfid.h"
#define ZIGBEE_DEV_PATH "/dev/ttyUSB0"

char m_aUsbKey[SIZE] = "0"; //获取按键键值
char m_aRelMat[SIZE] = "0";  //接受数据库匹配结果
char m_aIpAd[SIZE] = "0";//匹配数据库IP
char m_aRecvBuf[SIZE] = "0";//接受服务器的客户命令
char m_aRfidNu[SIZE] = "0";
const char c_aHoms[SIZE] = "homesuccess";//数据库匹配结果
const char c_aFris[SIZE] = "friendsuccess";
const char c_aHomf[SIZE] = "homefailed";
const char c_aFrif[SIZE] = "friendsfailed";

char m_aRelMat[SIZE];
int ttyUSB0_fd;
int fd, fw, fk, i;
void openDoor_BUZZ(int fw, int fre)//开门 蜂鸣响一声
{
	ioctl(fw, FSPWM_START, 0);//蜂鸣器开
	ioctl(fw, FSPWM_SET_FREQ, fre);
	usleep(300000);//3豪秒
	ioctl(fw, FSPWM_STOP, 0);//蜂鸣器关
}
void closeDoor_BUZZ(int fw, int fre)//关门 蜂鸣器响两声
{
	ioctl(fw, FSPWM_START, 0);//蜂鸣器开
	ioctl(fw, FSPWM_SET_FREQ, fre);
	usleep(300000);
	ioctl(fw, FSPWM_STOP, 0);//蜂鸣器关
	usleep(200000);
	ioctl(fw, FSPWM_START, 0);//蜂鸣器开
	ioctl(fw, FSPWM_SET_FREQ, fre);
	usleep(300000);
	ioctl(fw, FSPWM_STOP, 0);//蜂鸣器关
}

#define KEY3_ENTER 29

struct key_event{

	int code;
	int value;
};
struct key_event event;
void handle(int sig)
{
	int keyValue = 0;
	//收到信号，处理信号
	if (read(fd, &keyValue, sizeof(event.value)) > 0)
	{
		printf("key3 down, id=%d\r\n", event);
	}
}

int main()
{


int flag = 0;
	
	//open
	fk = open("/dev/key",O_RDWR);
	if(fk < 0){

		perror("open");
		exit(-1);
	}
	//安装中断 
	signal(SIGIO, handle);
	
	//设置属主
	fcntl(fd, F_SETOWN, getpid());
	
	//支持异步通信
	flag = fcntl(fd, F_GETFL); 
	fcntl(fd, F_SETFL, flag | FASYNC);
	
	
	while (1)
	{
		if (-1 == ttyUSB0_fd)
		{
			printf("open ttyUSB0 failed!\n");
		}

		fd = open("/dev/led0", O_RDWR);
		fw = open("/dev/pwm", O_RDWR);
		if (fd < 0 || fw < 0)
		{
			perror("open");
			exit(1);
		}
		ioctl(fd, LED5_ON);//初始化
		ioctl(fd, LED2_OFF);
		ioctl(fd, LED4_OFF);
		memset(m_aUsbKey, 0, sizeof(m_aUsbKey));
		memset(m_aRelMat, 0, sizeof(m_aRelMat));
		memset(m_aRfidNu, 0, sizeof(m_aRfidNu));
		
		ttyUSB0_fd = serial_init(ZIGBEE_DEV_PATH);
		int ret = zigbee_get_rfid_data(ttyUSB0_fd);
		if (ret < 0)
		{
			printf("no data!\n");
		}
		printf("rfid num = %s\n", m_aRfidNu);

		//a9_usb_keyboard();//获取键盘键值
		a9_sqlite3_match();//匹配数据库

		if (0 == strcmp(m_aRelMat, "homesuccess")) //led4亮开门延时10秒 关闭 之后led5亮
		{

			ioctl(fd, LED5_OFF);
			ioctl(fd, LED2_OFF);
			ioctl(fd, LED4_ON);
			openDoor_BUZZ(fw, 400);
			sleep(5);
			ioctl(fd, LED4_OFF);
			ioctl(fd, LED4_OFF);
			ioctl(fd, LED5_ON);
			closeDoor_BUZZ(fw, 400);

			close(fd);
			close(fw);
		}
		else if (0 == strcmp(m_aRelMat, "homefailed")) //led2亮  pwn 报警

		{

			ioctl(fd, LED5_ON);
			ioctl(fd, LED4_OFF);
			ioctl(fd, LED2_ON);

			ioctl(fw, FSPWM_START, 0);//蜂鸣器开
			ioctl(fw, FSPWM_SET_FREQ, 262);
			sleep(10);
			ioctl(fw, FSPWM_STOP, 0);//蜂鸣器关
			ioctl(fd, LED2_OFF);
		//安装中断 
			signal(SIGIO, handle);
			fd= open("/dev/led0", O_RDWR);
			fw = open("/dev/pwm", O_RDWR);
		if (fd < 0 || fw < 0)
		{
			perror("open");
			exit(1);
		}//操作之前门应该关
		ioctl(fd, LED5_ON);
	    ioctl(fd, LED2_OFF);
		ioctl(fd, LED4_OFF);
		memset(m_aUsbKey,0,sizeof(m_aUsbKey));
		memset(m_aRelMat,0,sizeof(m_aRelMat));
		a9_usb_keyboard();//获取键盘键值
		a9_sqlite3_match();//匹配数据库
		
		if(0 == strcmp(m_aRelMat,"homesuccess")) //led4亮开门延时10秒 关闭 之后led5亮
		{
			ioctl(fd, LED5_OFF);
			ioctl(fd, LED2_OFF);
			ioctl(fd, LED4_ON);
			openDoor_BUZZ(fw,400);
			sleep(10);
			ioctl(fd, LED4_OFF);
			ioctl(fd, LED5_ON);
			closeDoor_BUZZ(fw,400);	
		}
		else if (0 == strcmp(m_aRelMat,"homefailed"))//led2亮  pwn 报警
		{
		
			ioctl(fd, LED5_ON);
			ioctl(fd, LED4_OFF);
			ioctl(fd, LED2_ON);
			
			ioctl(fw,FSPWM_START,0);//蜂鸣器开
		    ioctl(fw,FSPWM_SET_FREQ,262);
			sleep(15);
			ioctl(fw,FSPWM_STOP,0);//蜂鸣器关
			ioctl(fd, LED2_OFF);
		
		}
		else if(0 == strcmp(m_aRelMat,"friendsuccess"))//led4亮开门 延时10秒 灭  led5亮关门
		{	
			printf("IP = %s\r\n",m_aIpAd);
			ContexA9_Client(m_aRelMat);//数据库和客户端连接
			if(0 == strcmp(m_aRecvBuf,"open"))
			{
				ioctl(fd, LED5_OFF);
				ioctl(fd, LED2_OFF);
		    	ioctl(fd, LED4_ON);
				openDoor_BUZZ(fw,400);
				sleep(10);
				ioctl(fd, LED4_OFF);
				ioctl(fd, LED5_ON);
				closeDoor_BUZZ(fw,400);
			}
			else if(0 == strcmp(m_aRecvBuf,"exit"))
			{
				ioctl(fd, LED5_ON);
				ioctl(fd, LED2_OFF);
		    		ioctl(fd, LED4_OFF);
				//openDoor_BUZZ(fw,400);
				//sleep(10);
				//ioctl(fd, LED4_OFF);
				//ioctl(fd, LED5_ON);
				closeDoor_BUZZ(fw,600);
			}
			memset(m_aIpAd, 0, SIZE);//清空 IP
			memset(m_aRecvBuf, 0, SIZE);//清空 QT服务器 发送的开门消息
			
		
		}
		else if (0 == strcmp(m_aRelMat,"friendfailed"))//led2亮  pwn 报警   ，led5亮关门
		{
			ioctl(fd, LED5_ON);
			ioctl(fd, LED4_OFF);
			ioctl(fd, LED2_ON);
		
			ioctl(fw,FSPWM_START,0);//蜂鸣器开
		    ioctl(fw,FSPWM_SET_FREQ,262);
			sleep(15);
			ioctl(fw,FSPWM_STOP,0);//蜂鸣器关
			
			ioctl(fd, LED2_OFF);
		
		}
			
			

		}
			close(fd);
			close(fw);
	}




		close(fk);

return 0;
}


