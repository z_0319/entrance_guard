/********************************************************************************
** Form generated from reading UI file 'user_control.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USER_CONTROL_H
#define UI_USER_CONTROL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_user_control
{
public:
    QPushButton *open;
    QPushButton *exit;
    QTextBrowser *movietostart;
    QPushButton *select;
    QLabel *label;
    QSlider *verticalSlider;
    QPushButton *pushButton;

    void setupUi(QWidget *user_control)
    {
        if (user_control->objectName().isEmpty())
            user_control->setObjectName(QStringLiteral("user_control"));
        user_control->resize(806, 520);
        open = new QPushButton(user_control);
        open->setObjectName(QStringLiteral("open"));
        open->setGeometry(QRect(570, 60, 151, 28));
        exit = new QPushButton(user_control);
        exit->setObjectName(QStringLiteral("exit"));
        exit->setGeometry(QRect(570, 140, 151, 28));
        movietostart = new QTextBrowser(user_control);
        movietostart->setObjectName(QStringLiteral("movietostart"));
        movietostart->setGeometry(QRect(30, 40, 491, 371));
        select = new QPushButton(user_control);
        select->setObjectName(QStringLiteral("select"));
        select->setGeometry(QRect(570, 220, 93, 28));
        label = new QLabel(user_control);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(580, 300, 141, 21));
        verticalSlider = new QSlider(user_control);
        verticalSlider->setObjectName(QStringLiteral("verticalSlider"));
        verticalSlider->setGeometry(QRect(740, 350, 22, 160));
        verticalSlider->setOrientation(Qt::Vertical);
        pushButton = new QPushButton(user_control);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(570, 270, 91, 31));

        retranslateUi(user_control);

        QMetaObject::connectSlotsByName(user_control);
    } // setupUi

    void retranslateUi(QWidget *user_control)
    {
        user_control->setWindowTitle(QApplication::translate("user_control", "user_control", 0));
        open->setText(QApplication::translate("user_control", "open to door", 0));
        exit->setText(QApplication::translate("user_control", "exit", 0));
        select->setText(QApplication::translate("user_control", "select", 0));
        label->setText(QString());
        pushButton->setText(QApplication::translate("user_control", "close", 0));
    } // retranslateUi

};

namespace Ui {
    class user_control: public Ui_user_control {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USER_CONTROL_H
