#include "dialog.h"
#include <QApplication>
#include "form.h" //注意添加上，否则无法使用

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Dialog w;
    //Widget  m;
    Form   m;

    if(w.exec()==QDialog::Accepted)//接收上一个界面发的信号
    {
        m.show();//显示新的窗体
        return a.exec();
    }
    else
        return 0;
}
