#-------------------------------------------------
#
# Project created by QtCreator 2017-05-23T17:15:39
#
#-------------------------------------------------

QT       += core gui network multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test22222222
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
    form.cpp \
    user_control.cpp

HEADERS  += dialog.h \
    form.h \
    ui_dialog.h \
    ui_form.h \
    user_control.h

FORMS    += dialog.ui \
    form.ui \
    user_control.ui
