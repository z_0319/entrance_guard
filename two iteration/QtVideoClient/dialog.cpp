#include "dialog.h"
#include "ui_dialog.h"
#include "form.h"
#include <QMessageBox>
#include "user_control.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    ui->lineEdit->setFocus();//光标跳到用户名栏开头处
    ui->lineEdit_2->setEchoMode(QLineEdit::Password);//让输入的密码成小黑点
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    //Form  *f=new  Form;
    //f->show();
   // this->hide();
    ui->lineEdit->setFocus();
    if(ui->lineEdit->text().trimmed()==tr("1")&&ui->lineEdit_2->text()==tr("1"))//验证用户名和密码是否正确
    {
        this->accept();//发一个信号给另一个界面
    }
    else
    {
        QMessageBox::warning(this,tr("warning"),tr("用户名和密码错误"),QMessageBox::Yes);//如果不正确，弹出警告信息
        ui->lineEdit->clear();//清空用户名栏
        ui->lineEdit_2->clear();//清空密码栏
        ui->lineEdit->setFocus();//光标跳至用户名栏开头处
    }
}

void Dialog::on_pushButton_2_clicked()
{
    this->close();
}


