#ifndef FORM_H
#define FORM_H

#include <QWidget>
#include <QTimer>
#include <QtNetwork/QTcpSocket>
#include <QTcpServer>
#include "user_control.h"

namespace Ui {
class Form;
}

class Form : public QWidget
{
    Q_OBJECT

public:
    explicit Form(QWidget *parent = 0);
    ~Form();
   // bool listen(const QHostAddress &address = QHostAddress::Any, quint16 port = 0);
private slots:
    void on_pushButton_clicked();
    void msconnected();
    void msdisconnected();
    void updatepic();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::Form *ui;
    QTcpSocket  *socket;
    QTimer  *timer;
    QPixmap *pixmap;
    class user_control dialog2;
};

#endif // FORM_H
