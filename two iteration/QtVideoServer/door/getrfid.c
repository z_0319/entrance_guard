#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>
#include <termios.h>

#include <error.h>
#include <pthread.h>

#include <sys/types.h>
#include <sys/stat.h>
#include "serial.h"
#include "getrfid.h"
#include "global.h"
//extern char m_aRelMat[SIZE];


int zigbee_get_rfid_data(int fd)
{
	 
	int ret;
	char buf[SIZE];
	
	memset(m_aRfidNu, 0, sizeof(m_aRfidNu));
	memset(buf, 0, sizeof(buf));
	ret = serial_recv_exact_nbytes(fd, buf, 36);
	if (ret == 36) 
	{
		printf("read success!\n");
		 
		memcpy(m_aRfidNu,buf+6,10);
			 
	}
	return ret;
}
