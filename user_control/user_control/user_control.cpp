#include "user_control.h"
#include "ui_user_control.h"
#include <QHostAddress>
#include <QFileDialog>
#include <QFileInfo>

user_control::user_control(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::user_control)
{
    ui->setupUi(this);
    user_tcpserver = new QTcpServer;
    user_tcpserver->listen(QHostAddress::Any,5212);
    connect(user_tcpserver,SIGNAL(newConnection()),this,SLOT(controlconnect()));

    player = new QMediaPlayer(this);
    ui->verticalSlider->setValue(50);

    connect(ui->select,SIGNAL(clicked(bool)),this,SLOT(selectRemindMusic()));
    connect(ui->verticalSlider,SIGNAL(valueChanged(int)),player,SLOT(setVolume(int)));


}

user_control::~user_control()
{
    delete ui;
}

void user_control::on_open_clicked()
{
    QString strdata = "open";
    QByteArray  arr = strdata.toLatin1();
    user_tcpsocket->write(arr);
    player->stop();
}

void user_control::on_exit_clicked()
{
    QString strdata = "over";
    QByteArray  arr = strdata.toLatin1();
    user_tcpsocket->write(arr);
    player->stop();
}

void user_control::controlconnect()
{
    user_tcpsocket = user_tcpserver->nextPendingConnection();
    connect(user_tcpsocket,SIGNAL(readyRead()),this,SLOT(recvMovieData()));
}

void user_control::recvMovieData()
{
    QString strdata = user_tcpsocket->readAll();
    ui->movietostart->setText(strdata);
    musicPath = "H:/朴树 - 在木星.mp3";
    player->setMedia(QUrl(musicPath));
    player->play();
    player->setVolume(50);
}
void user_control::selectRemindMusic()
{
    musicPath = QFileDialog::getOpenFileName(this,"open","./../user_control","MP3(*.mp3)");
    if(false ==musicPath.isEmpty())
    {
        player->setMedia(QUrl(musicPath));
        player->play();
        player->setVolume(50);

        QFileInfo info(musicPath);
        MusicName = info.fileName();
    }
}

