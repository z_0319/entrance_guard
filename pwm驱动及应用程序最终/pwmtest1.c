#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#define FSPWM_MAGIC	'f'
#define FSPWM_START	_IO(FSPWM_MAGIC, 0)
#define FSPWM_STOP	_IO(FSPWM_MAGIC, 1)
#define FSPWM_SET_FREQ	_IOW(FSPWM_MAGIC, 2, unsigned int)
#define data 10000

int main(int argc, char **argv)
{
	int fd;
	int i = 0;
	int ret;
	unsigned int freq;

	fd = open("/dev/pwm", O_RDWR);
	if (fd < 0) 
	{
		perror("open fault!");
		exit(1);
	}
	ret = ioctl(fd,FSPWM_START,0);//打开蜂鸣器
	
	ret = ioctl(fd,FSPWM_SET_FREQ,262);
	sleep(5);
	ret = ioctl(fd,FSPWM_STOP,0);
/*	while(1)
	{
		ret = ioctl(fd,FSPWM_SET_FREQ,262);
		sleep(10);
		ret = ioctl(fd,FSPWM_STOP,0);
		//data--;
	}
*/	
	return 0;
}
