#include <linux/module.h>
#include <linux/init.h>
#include <linux/of.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/of_irq.h>
#include <linux/slab.h>
#include <asm/io.h>
#include <asm/uaccess.h>
 
#define GPX1CON 0x11000c20
//#define KEY2_ENTER 25
#define KEY3_ENTER 26
 
struct key_event{
	int code;
	int value;
};
struct dev_desc{
	void *reg_addr;
	int key_major;	
//	unsigned int irqno1;
	unsigned int irqno2;
	struct class *cls;
	struct device *dev;
	struct key_event event;
};
 
struct dev_desc *key_dev;
/*
//中断处理函数
irqreturn_t key2_handler(int irqno, void *dev)
{
	unsigned int value;
	printk("-----------------%s--------------------\n",__FUNCTION__);
	//读取key2的状态
	value = readl(key_dev->reg_addr+4) & (0x1<<1);
	printk("value = %d\n",value);
	if(value){
		key_dev->event.code = KEY2_ENTER;
		key_dev->event.value = 1;
		printk("key2 up\n");
	}
	else
	{
		key_dev->event.code = KEY2_ENTER;
		key_dev->event.value = 0;
		printk("key2 down\n");
	}
	
	return IRQ_HANDLED;
}*/

irqreturn_t key3_handler(int irqno, void *dev)
{
 
	unsigned int value;
	printk("-----------------%s--------------------\n",__FUNCTION__);
	//读取key3的状态
	value = readl(key_dev->reg_addr+4) & (0x1<<2);
	printk("value = %d\n",value);
	if(value){
		key_dev->event.code = KEY3_ENTER;
		key_dev->event.value = 1;
		printk("key3 up\n");
	}
	else
	{
		key_dev->event.code = KEY3_ENTER;
		key_dev->event.value = 0;
		printk("key3 down\n");
	}
	
	return IRQ_HANDLED;
}
 
ssize_t key_read (struct file *filp, char __user *buf, size_t count, loff_t *fpos)
{
	int ret;
	
	ret = copy_to_user(buf, &key_dev->event, count);
	if(ret){
		printk("copy_from_user failed\n");
		return -EFAULT;
	}
 
	memset(&key_dev->event, 0, sizeof(key_dev->event));
	
	return count;
}
 
ssize_t key_write (struct file *filp, const char __user *buf, size_t count, loff_t *fpos)
{
	printk("-----------------%s--------------------\n",__FUNCTION__);
 
	return 0;
}
 
int key_open (struct inode *inode, struct file *filp)
{
	printk("-----------------%s--------------------\n",__FUNCTION__);
 
	return 0;
}
 
int key_close (struct inode *inode, struct file *filp)
{
	printk("-----------------%s--------------------\n",__FUNCTION__);
 
	return 0;
}
 
const struct file_operations fops = {
	.open = key_open,
	.read = key_read,
	.write = key_write,
	.release = key_close,
};
 
static int __init key_dev_init(void)
{
	struct device_node *np1,*np2;
	int err = 0;
 
	printk("-----------------%s--------------------\n",__FUNCTION__);
	//分配空间
	key_dev = kzalloc(sizeof(struct dev_desc), GFP_KERNEL);
	if(key_dev == NULL){
		printk("kzalloc failed\n");
		return -ENOMEM;
	}
/*	//获取key2的设备树节点
	np1 = of_find_node_by_path("/key2_node");
	if(np1 == NULL){
		printk("key2 of_find_node_by_path failed\n");
	}
*/
	//获取key3的设备树节点
	np2 = of_find_node_by_path("/key3_node");
	if(np2 == NULL){
		printk("key3 of_find_node_by_path failed\n");
	}
	
	//获取key2中断号
//	key_dev->irqno1 = irq_of_parse_and_map(np1,0);
	//获取key3中断号
	key_dev->irqno2 = irq_of_parse_and_map(np2,0);
	
	//打印中断号
//	printk("irqno of key2 is %d\n",key_dev->irqno1);
	printk("irqno of key3 is %d\n",key_dev->irqno2);
 
	//申请主设备号
	key_dev->key_major = register_chrdev(0, "key_drv",&fops);
	if(key_dev->key_major < 0){
		printk("register_chrdev failed\n");
		err = -ENODEV;
		goto err_0;
	}
	//创建设备节点
	key_dev->cls = class_create(THIS_MODULE, "key_class");
	if(IS_ERR(key_dev->cls)){
		printk(KERN_ERR "class_create failed\n");
		err = PTR_ERR(key_dev->cls);
		goto err_1;
	}
	key_dev->dev = device_create(key_dev->cls, NULL, MKDEV(key_dev->key_major, 0), NULL, "key");
	if(IS_ERR(key_dev->dev))
	{
		printk(KERN_ERR "device_create failed\n");
		err = PTR_ERR(key_dev->dev);
		goto err_2;
	}
	printk("-----request_irq-----\n");
 //key2申请中断
/*	if(request_irq(key_dev->irqno1,key2_handler,IRQF_TRIGGER_FALLING|IRQF_TRIGGER_RISING,"key2_irq",NULL))
	{
		printk("key2 request_irq failed\n");
	}
	*/
	//key3申请中断
	if(request_irq(key_dev->irqno2,key3_handler,IRQF_TRIGGER_FALLING|IRQF_TRIGGER_RISING,"key3_irq",NULL))
	{
		printk("key3 request_irq failed\n");
	}
	//将GPX1CON物理地址映射为虚拟地址reg_addr
	key_dev->reg_addr = ioremap(GPX1CON,8);
	if(key_dev->reg_addr == NULL)
	{
		printk("ioremap error\n");
		err = -EFAULT;
		goto err_3;
	}
	return 0;
	
err_3:
	device_destroy(key_dev->cls,MKDEV(key_dev->key_major, 0));
err_2:
	class_destroy(key_dev->cls);
err_1:
	unregister_chrdev(key_dev->key_major, "key_drv");
err_0:
	kfree(key_dev);
	return err;
}
 
static void __exit key_dev_exit(void)
{
	printk("-----------------%s--------------------\n",__FUNCTION__);
 
	//释放地址映射
	iounmap(key_dev->reg_addr);
	//释放key2中断
//	free_irq(key_dev->irqno1,NULL);
	//释放key3中断
	free_irq(key_dev->irqno2,NULL);
	//释放设备节点
	device_destroy(key_dev->cls,MKDEV(key_dev->key_major, 0));
	class_destroy(key_dev->cls);
	//释放设备号
	unregister_chrdev(key_dev->key_major, "key_drv");
	//释放key_dev
	kfree(key_dev);
}
 
module_init(key_dev_init);
module_exit(key_dev_exit);
MODULE_LICENSE("GPL");
 
 
 
 
 
 
